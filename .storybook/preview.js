
import { createRoot } from 'solid-js';
document.getElementsByTagName('html')[0].setAttribute('dir', 'ltr');
export const decorators = [
  storyFn =>
    createRoot(() => (
      <div>
        <div>{storyFn()}</div>
      </div>
    )),
];
