// other webpack config: https://github.com/psychobolt/react-rollup-boilerplate/blob/master/.storybook/webpack.config.js
// see latest storybook config: https://github.com/joepuzzo/informed
// ring-ui is using storybook

// const postcssPresetEnv = require('postcss-preset-env');
// const postcssImport = require('postcss-import');
// const postcssIested = require('postcss-nested');
// const Autoprefixer = require('autoprefixer');
// const PostCSSAssetsPlugin = require('postcss-assets-webpack-plugin');
// const PostCSSCustomProperties = require('postcss-custom-properties');
const path = require('path');
// const rtl = require('postcss-rtl');
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ThemePlugin = require('./postcss/ThemePlugin');
module.exports = {
  stories: ['../stories/**/*.stories.[jt]sx'],
  addons: ['@storybook/addon-essentials'],
  // addons: [
  // // '@storybook/preset-typescript',
  // // '@storybook/addon-actions',
  // // '@storybook/addon-links',
  // // '@storybook/addon-knobs',
  // // '@storybook/addon-notes',
  // // '@storybook/addon-viewport',
  // // '@storybook/addon-storysource',
  // // '@storybook/addon-a11y'
  // ],
  webpackFinal: async config => {
    config.resolve.alias = {
      ...config.resolve.alias,
      //react: 'solid-js/web',
      //'react-dom': 'solid-js/web'
    };

    config.module.rules = [
      {
        test: /\.md$/,
        use: [
          {
            loader: 'raw-loader',
          },
        ],
      },
      // {
      //   test: /\.html$/,
      //   use: [
      //     {
      //       loader: 'html-loader',
      //     },
      //   ],
      // },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              postcssOptions: {
                config: './.storybook/',
              },
            },
          },
        ],
      },
      {
        test: /\.(svg|ico|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|cur|ani|pdf)(\?.*)?$/,
        use: ['file-loader'],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader'],
      },
      {
        test: /\.scss$/,
        use: [
          require.resolve('style-loader'),
          {
            loader: 'css-loader',
            options: {
              modules: true,
              sourceMap: true,
              importLoaders: 1,
            },
          },
          // Loader for webpack to process CSS with PostCSS
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              postcssOptions: {
                config: './.storybook/',
              },
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],

        include: path.resolve(__dirname, '../'),
      },
      {
        test: /\.(js|jsx|ts|tsx)$/,
        loader: require.resolve('babel-loader'),
        options: {
          plugins: ['@babel/plugin-proposal-class-properties'], //required  private id_ = 0;
          presets: ['solid', '@babel/preset-typescript', '@babel/preset-env'], // , 'linaria/babel'
          sourceMap: false,
        },
      },
      // Not works
      // {
      //   loader: 'linaria/loader',
      //   options: {
      //     sourceMap: process.env.NODE_ENV !== 'production',
      //   },
      // },
    ];

    // Taken from terra source:
    /*config.plugins.push(
      new PostCSSAssetsPlugin({
        test: /\.(scss|css)$/,
        log: false,
        plugins: [
          PostCSSCustomProperties({
            preserve: true,
            // If we have a theme file, use the webpack promise to webpack it.  This promise will resolve to
            // an object with themeable variables and values. This will then be used to update the end state CSS
            // so that they are populated with values if variables aren't supported (e.g. IE10). This dance is
            // necessary when code splitting to ensure the variables and values are applied across all code split
            // css files
            //...themeFile && { importFrom: [getThemeWebpackPromise(rootPath, themeFile, themeConfig)] },
          }),
        ],
      }),
    );*/
    /*config.module.rules.push({
      test: /\.svg$/,
      use: [
        {
          loader: 'babel-loader',
          options: {
            presets: ['solid', '@babel/env'],
          },
        },
        {
          loader: '@svgr/webpack',
          options: {
            icon: true,
            babel: false,
          },
        },
      ],
    });*/
    //1
    // config.module.rules.push({
    //   test: /\.(ts|tsx)$/,
    //   loader: require.resolve('babel-loader'),
    //   options: {
    //     plugins: ['@babel/plugin-proposal-class-properties'],
    //     presets: ['solid', '@babel/preset-typescript', '@babel/preset-env'],
    //   },
    // });
    // only scss
    // config.module.rules.push({
    //   test: /\.scss$/,
    //   use: ['style-loader', 'css-loader', 'sass-loader'],
    //   include: path.resolve(__dirname, '../'),
    // });
    //2
    // sass with css_modules:
    // https://github.com/storybookjs/storybook/issues/2320
    // config.module.rules.push({
    //   test: /\.(scss)$/,
    //   use: [
    //     require.resolve('style-loader'),
    //     {
    //       loader: 'css-loader',
    //       options: {
    //         modules: true,
    //         sourceMap: true,
    //         importLoaders: 1,
    //       },
    //     },
    //     {
    //       loader: 'postcss-loader',
    //       options: {
    //         // Add unique ident to prevent the loader from searching for a postcss.config file. See: https://github.com/postcss/postcss-loader#plugins
    //         ident: 'postcss',
    //         sourceMap: true,
    //         plugins: [ThemePlugin({}), rtl(), Autoprefixer()],
    //         // config: {path: './.storybook/',  },
    //       },
    //     },
    //     {
    //       loader: 'sass-loader',
    //       options: {
    //         sourceMap: true,
    //       },
    //     },
    //   ],
    // });

    config.resolve.extensions.push('.ts', '.tsx');
    return config;
  },
};
/*import { addDecorator, configure } from '@storybook/html';
import { createRoot } from 'solid-js';
import { Base } from '../src/components/UI/core/base/src/Base';
document.getElementsByTagName('html')[0].setAttribute('dir', 'ltr');
addDecorator(story => {
  return createRoot(() => (
    <Base>
      <div dir="ltr">{story()}</div>
    </Base>
  ));
});
*/
// use actions: https://storybook.js.org/docs/react/essentials/actions
