import { createState, onCleanup } from 'solid-js';
interface IScrollButtonProps extends JSX.HTMLAttributes<Element> {
  delayInMs?: any;
  scrollStepInPx?: any;
}
type ScrollButtonState = {
  intervalId: number;
  isVisible: boolean;
};
export const ScrollButton = (props: IScrollButtonProps) => {
  const [state, setState] = createState({ intervalId: 0, isVisible: false } as ScrollButtonState);
  window.onscroll = function () {
    scrollFunction();
  };
  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      setState({ isVisible: true });
    } else {
      setState({ isVisible: false });
    }
  }
  onCleanup(() => {
    window.onscroll = null;
  });
  function scrollToTopFast() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  return (
    <button
      title="Back to top"
      style={`display: ${state.isVisible ? 'block' : 'none'}`}
      onClick={scrollToTopFast}
    >Top</button>
  );
};
